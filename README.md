# Front-end AngularJS crée par DSI-1

Dans le cadre du projet de filière, nous devons réaliser une application web, mobile et Android.
Voici le Front-end de l'application Web, développée avec le framework AngularJS.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installation

A step by step series of examples that tell you have to get a development env running

Say what the step will be

Mettre en place l'univers Git :

```
npm init
```

Installer npm :

```
npm install
```

Installer http-server sur en "général"

```
http install http-server -g
```

Lancer l'application, pour la faire démarrer :

```
http-server
```

Regarder sur quelle adresse http, l'application est -elle en train de tourner (en général, il s'agit de http://localhost:8080)
