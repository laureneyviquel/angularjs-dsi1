/**
*
* Controlleur de l'onglet utilisateur
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/

(function() {

    "use strict";

    angular
        .module("ngMateriels")

    .controller("userCtrl", function($scope, $state, $http, AuthenticationService, $mdDialog, $mdSidenav, $window, $localStorage, $rootScope, $location) {

        if ($localStorage.currentUser.role === 1) {
            $scope.user;

            // get the json data from users passed throw the back end thanks to the factories
            AuthenticationService.getUsers().then(function(response) {
                $scope.users = response.data.response;
            });

            $scope.openSidebar = function() {
                $mdSidenav('left').open();
            }

            $scope.closeSidebar = function() {
                $scope.user = {}; // to clear the field in the form
                $mdSidenav('left').close();
            }


            // Pour Créer un utilisateur : ouvre la sidebar et initialise les champs dans le formulaire
            $scope.onBtCreateUser = function() {
                $scope.editing = false;
                $scope.user = {}; // to clear the field in the form
                $scope.openSidebar();
            }

            // Créer 1 nouvel utilisateur, lorsqu'on clique sur enregistrer dans le formulaire
            $scope.createUser = function(user) {
                console.log("button");
                console.log(user);

                AuthenticationService.createUser(user, function() {
                    $window.location.reload();

                    $scope.user = {}; // to clear the field in the form
                    $scope.closeSidebar();
                }, function(error) {
                    console.log(error);
                });


            };

            // Supprimer 1 utilisateur
            $scope.deleteUser = function(event, user) {
                // Fenetre de dialogue pour confirmer si supprésion de l'utilisateur
                var confirm = $mdDialog.confirm()
                    .title('Etes-vous sûr de vouloir supprimer : ' + user.prenom + '?')
                    .ok('Oui')
                    .cancel('Non')
                    .targetEvent(event);

                $mdDialog.show(confirm).then(function() {
                    console.log("supprime id : " + user.id);
                    //appelle la fonction http du back-end pour supprimer l'utilisateur, en passant son id en paramètre

                    AuthenticationService.deleteUser(user.id, function() {
                        $window.location.reload();
                    }, function(error) {
                        console.log(error);
                    });


                }, function() {});
            }

            // Pour modifier un utilisateur : ouvre la sidebar et initialise les valeurs des champs dans le formulaire
            $scope.onBtEditUser = function(user) {
                console.log(user);
                $scope.editing = true;
                $scope.openSidebar();
                //console.log("nom à editer  : " + user.nom);
                $scope.user = user; //initialise l'utilisateur qu'on a selectionner pour le mettre dans les champs du form automatiquement
            }

            // Modifie 1 utilisateur, lorsqu'on clique sur enregistrer les modifications dans le formulaire
            $scope.editUser = function(user) {
                $scope.editing = false;
                console.log(user);

                //editer ici la fonction avec appel à la factorie
                AuthenticationService.editTheUser(user, function(result) {
                    if (result === true) {
                        console.log("super");
                    } else {}
                });
                // reload la requete get pour avoir les nouveaux utilisateurs
                //$window.location.reload();

                $scope.user = {}; // to clear the field in the form
                $scope.closeSidebar();
            }

            // Afficher quelques détails sur l'utilisateur lorqu'on clique sur la ligne de la "liste des utilisateurs"
            $scope.goToPerson = function(person, event) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .title('Informations de l utilisateur ' + person.nom)
                    .textContent('Email : ' + person.email)
                    .ariaLabel('Person inspect demo')
                    .ok('Ok!')
                    .targetEvent(event)
                );
            };
        } else {
            console.log("Controlleur non authorisé");
        }


    });
})();
