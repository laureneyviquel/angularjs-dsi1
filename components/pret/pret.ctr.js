/**
*
* Controlleur de l'onglet pret
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/

(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("pretCtrl", function($scope, $state, $http, AuthenticationService, $element, $q, $localStorage) {

            if ($localStorage.currentUser.role === 1 || $localStorage.currentUser.role === 2) {
                // get the json data from the Q1 excel file passed throw the back end thanks to the factories
                AuthenticationService.getPret().then(function(response) {
                    $scope.prets = response.data.response;
                });

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Id matériel",
                    field: "idObjet",
                    width: 110
                }, {
                    headerName: "Catégorie",
                    field: "nom"
                }, {
                    headerName: "Nom",
                    field: "APPRENANT_NOM"
                }, {
                    headerName: "Prénom",
                    field: "APPRENANT_PRENOM"
                }, {
                    headerName: "Départ du prêt",
                    field: "depart",
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Retour prévu",
                    field: "retourPrevu",
                    valueFormatter: dateFormatter
                }];

                // Configurations de l'ag-grid
                $scope.gridOptions = {
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                };

                // Initialize les données du tableaux via l'api du back-end
                AuthenticationService.getPret().then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response);
                });

                // exporter les données dans le tableau en csv, lorsqu'on clique sur le bouton "exporter vers csv"
                $scope.onBtExport = function() {
                    $scope.gridOptions.api.exportDataAsCsv();
                }

                // permet de formater la date
                function dateFormatter(params) {
                    var date = new Date(params.value);
                    var returnValue = date.toLocaleString()
                    if (returnValue === "Invalid Date") {
                        return " ";
                    } else {
                        return returnValue;
                    }
                }

            } else {
                console.log("Controlleur non authorisé");
            }





        });
})();
