/**
* Permet de gérer les factories et les services angualaJS
* pour toute l'application
* Effectue toutes les requetes http vers le back-end : http://193.70.38.152:3000
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-07
*/


(function() {
    'use strict';

    angular
        .module('ngMateriels')
        .factory('AuthenticationService', Service);

    function Service($http, $localStorage) {
        var service = {};

        //Général
        service.Login = Login;
        service.Logout = Logout;

        //Page alerte
        service.getAlertes = getAlertes;
        service.getHistorique = getHistorique;
        service.deleteObjet = deleteObjet;

        //Page user
        service.getUsers = getUsers;
        service.createUser = createUser;
        service.editTheUser = editTheUser;
        service.deleteUser = deleteUser;

        //Page stock
        service.getStock = getStock;
        service.createLimite = createLimite;
        service.editLimite = editLimite;
        service.createCategorie = createCategorie;

        //Page pret
        service.getPret = getPret;

        //Page matériel
        service.getObjets = getObjets;
        service.getMateriels = getMateriels;

        return service;


        /**
        *
        * Général
        *
        */

        // Se connecter à l'application
        function Login(email, password, callback) {
            $http({
                method: 'POST',
                url: 'http://193.70.38.152:3000/users/connexion',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    email: email,
                    password: password
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);
                console.log("token : " + response.data.token);

                // login successful if there's a token in the response
                if (response.data.token) {
                    console.log("nom : " + response.data.user[0].nom);
                    // store email and token in local storage to keep user logged in between page refreshes
                    $localStorage.currentUser = {
                        email: response.data.user[0].email,
                        nom: response.data.user[0].nom,
                        prenom: response.data.user[0].prenom,
                        role: response.data.user[0].role,
                        token: response.data.token
                    };

                    console.log("role : " + $localStorage.currentUser.role);

                    // add jwt token to auth header for all requests made by the $http service
                    $http.defaults.headers.common.Authorization = 'Bearer ' + response.data.token;

                    // execute callback with true to indicate successful login
                    callback(true);
                } else {
                    // execute callback with false to indicate failed login
                    callback(false);
                }

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);

            });
        }

        // Se déconnecter de l'application
        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }

        /**
        *
        * Page Alerte
        *
        */

        // Récupérer tous les alertes de stock et pret du matériel
        function getAlertes() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/alertes',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        // Afficher l'historique d'1 matériel
        function getHistorique(id) {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/objets/'+ id + '/historique',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        // Supprimer 1 objet (matériel), en passant son id en paramètre
        function deleteObjet(id) {
            return $http({
                method: 'DELETE',
                url: 'http://193.70.38.152:3000/objets/' + id,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        /**
        *
        * Page Utilisateur
        *
        */

        // Récupérer tous les utilisateurs de l'application
        function getUsers() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/users',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        // Créer 1 utilisateur
        function createUser(user, success, error) {
            return $http({
                method: 'POST',
                url: 'http://193.70.38.152:3000/users',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: user
            }).then(function successCallback(response) {
                success();

            }, function errorCallback(response) {
                error(response);

            });
        }

        // Modifier les données d'1 utilisateur
        function editTheUser(user, callback) {
            return $http({
                method: 'PATCH',
                url: 'http://193.70.38.152:3000/users/' + user.id,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: user
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);

            });
        }

        // Supprimer 1 utilisateur, en passant son id en paramètre
        function deleteUser(id, success, error) {
            return $http({
                method: 'DELETE',
                url: 'http://193.70.38.152:3000/users/' + id,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            }).then(function successCallback(response) {
              console.log(response);
                success();

            }, function errorCallback(response) {
                error(response);

            });
        }

        /**
        *
        * Page Stock
        *
        */

        // Récupérer tous les objets dans l'inventaire de stock
        function getStock() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/cat/stocks',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        // Créer 1 limite
        function createLimite(limite, idCategorie, siteEPF, callback) {
            return $http({
                method: 'POST',
                url: 'http://193.70.38.152:3000/climit',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    limite: limite,
                    idCategorie: idCategorie,
                    siteEPF: siteEPF
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);

            });
        }

        // Modifier la limite de matériel par catégorie et par site
        function editLimite(limite, idCategorie, siteEPF, callback) {
            return $http({
                method: 'PATCH',
                url: 'http://193.70.38.152:3000/climit/updatebackoffice',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    limite: limite,
                    idCategorie: idCategorie,
                    siteEPF: siteEPF
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);

            });
        }

        // Créer 1 catégorie
        function createCategorie(categorie, success, error) {
            return $http({
                method: 'POST',
                url: 'http://193.70.38.152:3000/cat',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    nom: categorie
                }
            }).then(function successCallback(response) {
              console.log(response);
                success();

            }, function errorCallback(response) {
                error(response);

            });
        }

        /**
        *
        * Page Pret
        *
        */

        // Récupérer tous les objets dans l'inventaire de stock
        function getPret() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/cat/prets',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }

        /**
        *
        * Page Matériel
        *
        */

        // Récupérer l’inventaire de tout le matériel stock et pret
        function getObjets() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/objets',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }


        // Récupérer l’inventaire du matériel en stock sur les trois sites de l’EPF
        function getMateriels() {
            return $http({
                method: 'GET',
                url: 'http://193.70.38.152:3000/extern',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'x-access-token': $localStorage.currentUser.token
                }
            })
        }


    }
})();
