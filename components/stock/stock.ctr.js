/**
*
* Controlleur de l'onglet stock
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/

(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("stockCtrl", function($scope, $state, $http, $location, AuthenticationService, $localStorage, $mdSidenav, $window) {

            if ($localStorage.currentUser.role === 1 || $localStorage.currentUser.role === 2) {

                $scope.objets;

                // get the json data from the Q1 excel file passed throw the back end thanks to the factories
                AuthenticationService.getStock().then(function(response) {
                    $scope.objets = response.data;
                });

                console.log($scope.objets);

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Matériel",
                    field: "materiel_stock",
                    width: 190,
                    pinned: 'left'
                }, {
                    headerName: 'Etat',
                    children: [{
                        headerName: 'Sceaux',
                        field: 'etat_sceaux',
                        valueParser: numberParser,
                        cellClassRules: {
                            'rag-red': 'x < 2', //si plus petit que 3 alors affiche la cellule en rouge
                            'rag-amber': 'x >= 2 && x < 5', //si entre 3 et 5 alors affiche la cellule en orange
                        }
                    }, {
                        headerName: 'Troyes',
                        field: 'etat_troyes',
                        valueParser: numberParser,
                        cellClassRules: {
                            'rag-red': 'x < 2', //si plus petit que 3 alors affiche la cellule en rouge
                            'rag-amber': 'x >= 2 && x < 5', //si entre 3 et 5 alors affiche la cellule en orange
                        }
                    }, {
                        headerName: 'Montpellier',
                        field: 'etat_montpellier',
                        valueParser: numberParser,
                        cellClassRules: {
                            'rag-red': 'x < 2', //si plus petit que 3 alors affiche la cellule en rouge
                            'rag-amber': 'x >= 2 && x < 5'
                        }
                    }, ]
                }, {
                    headerName: 'Mini',
                    children: [{
                        colId: 1,
                        headerName: 'Sceaux',
                        field: 'mini_sceaux',
                        valueParser: numberParser,
                        editable: true,
                        component: 'numericCellEditor'
                    }, {
                        colId: 2,
                        headerName: 'Troyes',
                        field: 'mini_troyes',
                        valueParser: numberParser,
                        editable: true,
                        component: 'numericCellEditor'
                    }, {
                        colId: 3,
                        headerName: 'Montpellier',
                        field: 'mini_montpellier',
                        valueParser: numberParser,
                        editable: true,
                        component: 'numericCellEditor'
                    }]
                }, {
                    headerName: 'Besoin',
                    children: [{
                        headerName: 'Sceaux',
                        field: 'besoin_sceaux',
                    }, {
                        headerName: 'Troyes',
                        field: 'besoin_troyes',
                    }, {
                        headerName: 'Montpellier',
                        field: 'besoin_montpellier',
                    }]
                }, {
                    headerName: "Commande",
                    field: "total_besoins",
                    pinned: 'right',
                    valueParser: numberParser,
                    cellClassRules: {
                        'rag-red': 'x < 2', //si plus petit que 3 alors affiche la cellule en rouge
                        'rag-amber': 'x >= 2 && x < 6', //si entre 3 et 5 alors affiche la cellule en orange
                        'rag-green': 'x >= 6' //si plus grand ou égal à 6 alors affiche la cellule en vert
                    }
                }];

                // exporter les données dans le tableau en csv, lorsqu'on clique sur le bouton "exporter vers csv"
                $scope.onBtExport = function() {
                    $scope.gridOptions.api.exportDataAsCsv();
                }

                //Pour le Cell styling de ag-grid --> couleurs dans dans la grille
                function numberParser(params) {
                    var newValue = params.newValue;
                    var valueAsNumber;
                    if (newValue === null || newValue === undefined || newValue === '') {
                        valueAsNumber = null;
                    } else {
                        valueAsNumber = parseFloat(params.newValue);
                    }
                    return valueAsNumber;
                }


                $scope.gridOptions = {
                    // we are using angular in the templates
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    defaultColDef: {
                        width: 110
                    },
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                    onRowEditingStarted: function(event) {
                        console.log('never called - not doing row editing');
                    },
                    onRowEditingStopped: function(event) {
                        console.log('never called - not doing row editing');
                    },
                    onCellEditingStarted: function(event) {
                        console.log("ancienne limite " + event.value);
                        $scope.ancienneLimite = event.value;
                        console.log('cellEditingStarted');
                    },
                    onCellEditingStopped: function(event) {
                        console.log(event);
                        console.log("ancienne limite : " + $scope.ancienneLimite);
                        console.log("limite : " + event.value);
                        console.log("id-categorie : " + event.data.id);
                        console.log("siteEPF : " + event.colDef.colId);

                        if ($scope.ancienneLimite === 0) { //crée la limite
                            console.log("Creer limite");
                            AuthenticationService.createLimite(event.value, event.data.id, event.colDef.colId, function(result) {
                                if (result === true) {
                                    console.log("super");
                                } else {}
                            });

                        } else if (event.value === NaN) { //erreur dans le type saisie (ce n'est pas un nombre)
                            console.log("erreur: type saisie n'est pas un nombre");

                        } else { //modifie la limite
                            console.log("Creer limite");
                            AuthenticationService.editLimite(event.value, event.data.id, event.colDef.colId, function(result) {
                                if (result === true) {
                                    console.log("super");
                                } else {}
                            });
                        }


                        console.log('cellEditingStopped');
                    },
                    components: {
                        numericCellEditor: NumericCellEditor
                    }
                };

                function onBtStopEditing() {
                    gridOptions.api.stopEditing();
                }

                function onBtStartEditing(key, char, pinned) {
                    gridOptions.api.setFocusedCell(0, 'lastLame', pinned);

                    gridOptions.api.startEditingCell({
                        rowIndex: 0,
                        colKey: 'lastName',
                        // set to 'top', 'bottom' or undefined
                        rowPinned: pinned,
                        keyPress: key,
                        charPress: char
                    });
                    console.log("onBtStopEditing");
                }

                function getCharCodeFromEvent(event) {
                    event = event || window.event;
                    return (typeof event.which == "undefined") ? event.keyCode : event.which;
                }

                function isCharNumeric(charStr) {
                    return !!/\d/.test(charStr);
                }

                function isKeyPressedNumeric(event) {
                    var charCode = getCharCodeFromEvent(event);
                    var charStr = String.fromCharCode(charCode);
                    return isCharNumeric(charStr);
                }

                // simple function cellRenderer, just returns back the name of the country
                function CountryCellRenderer(params) {
                    return params.value.name;
                }

                // function to act as a class
                function NumericCellEditor() {}

                // gets called once before the renderer is used
                NumericCellEditor.prototype.init = function(params) {
                    // create the cell
                    this.eInput = document.createElement('input');

                    if (isCharNumeric(params.charPress)) {
                        this.eInput.value = params.charPress;
                    } else {
                        if (params.value !== undefined && params.value !== null) {
                            this.eInput.value = params.value;
                        }
                    }

                    var that = this;
                    this.eInput.addEventListener('keypress', function(event) {
                        if (!isKeyPressedNumeric(event)) {
                            that.eInput.focus();
                            if (event.preventDefault) event.preventDefault();
                        } else if (that.isKeyPressedNavigation(event)) {
                            event.stopPropagation();
                        }
                    });

                    // only start edit if key pressed is a number, not a letter
                    var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
                    this.cancelBeforeStart = charPressIsNotANumber;
                };

                NumericCellEditor.prototype.isKeyPressedNavigation = function(event) {
                    return event.keyCode === 39 ||
                        event.keyCode === 37;
                };


                // gets called once when grid ready to insert the element
                NumericCellEditor.prototype.getGui = function() {
                    return this.eInput;
                };

                // focus and select can be done after the gui is attached
                NumericCellEditor.prototype.afterGuiAttached = function() {
                    this.eInput.focus();
                };

                // returns the new value after editing
                NumericCellEditor.prototype.isCancelBeforeStart = function() {
                    return this.cancelBeforeStart;
                };

                // example - will reject the number if it contains the value 007
                // - not very practical, but demonstrates the method.
                NumericCellEditor.prototype.isCancelAfterEnd = function() {
                    var value = this.getValue();
                    return value.indexOf('007') >= 0;
                };

                // returns the new value after editing
                NumericCellEditor.prototype.getValue = function() {
                    return this.eInput.value;
                };

                // any cleanup we need to be done here
                NumericCellEditor.prototype.destroy = function() {
                    // but this example is simple, no cleanup, we could  even leave this method out as it's optional
                };

                // if true, then this editor will appear in a popup
                NumericCellEditor.prototype.isPopup = function() {
                    // and we could leave this method out also, false is the default
                    return false;
                };


                AuthenticationService.getStock().then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response);
                    console.log(response.data.response);
                });



                ////////Créer une catégorie ////////////////////////////////////////////////////////////////////////////////////
                $scope.openSidebar = function() {
                    $mdSidenav('left').open();
                }

                $scope.closeSidebar = function() {
                    //$scope.categorie = {}; // to clear the field in the form
                    $mdSidenav('left').close();
                }


                // Pour Créer une catégorie : ouvre la sidebar et initialise les champs dans le formulaire
                $scope.onBtCreateCategorie = function() {
                    //$scope.editing = false;
                    //$scope.categorie = {}; // to clear the field in the form
                    $scope.openSidebar();
                }

                // Créer 1 nouvelle catégorie, lorsqu'on clique sur enregistrer dans le formulaire
                $scope.createCategorie = function(categorie) {
                    console.log("button");
                    console.log(categorie);

                    AuthenticationService.createCategorie(categorie, function(success) {
                        $window.location.reload();

                        //$scope.user = {}; // to clear the field in the form
                        //console.log(success);
                        $scope.closeSidebar();
                    }, function(error) {
                        console.log(error);
                    });


                };



            } else {
                console.log("Controlleur non authorisé");
            }


        });
})();
