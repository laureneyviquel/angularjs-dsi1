/**
*
* Controlleur de l'onglet accueil
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/

(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("homeCtrl", function($scope, $state, $http, AuthenticationService) {

            $scope.imagePath = '../img/epf_sceaux.jpg';

            initController();

            function initController() {}
        });
})();
