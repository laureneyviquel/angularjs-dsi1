/**
*
* Controlleur de l'onglet Matériel
* seulement losqu'il s'agit de l'administration est connectée à l'application
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/


(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("materielAdminCtrl", function($scope, $state, $http, $location, AuthenticationService, $mdDialog, $localStorage) {


            // get the json data from the Q1 excel file passed throw the back end thanks to the factories
            AuthenticationService.getMateriels().then(function(response) {
                $scope.materiels = response.data.response;
            });

            // Initialize the grid from the library : ag-grid
            var columnDefs = [{
                headerName: "Catégorie",
                field: "materiel_stock"
            }, {
                headerName: "Sceaux",
                field: "etat_sceaux"
            }, {
                headerName: "Troyes",
                field: "etat_troyes"
            }, {
                headerName: "Montpellier",
                field: "etat_montpellier",
            }];

            // Configurations de l'ag-grid
            $scope.gridOptions = {
                debug: true,
                angularCompileRows: true,
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                enableColResize: true,
                enableFilter: true,
                floatingFilter: true,
                domLayout: 'autoHeight',
            };

            // Initialize les données du tableaux via l'api du back-end
            AuthenticationService.getMateriels().then(function(response) {
                $scope.gridOptions.api.setRowData(response.data.response);
            });

            // exporter les données dans le tableau en csv, lorsqu'on clique sur le bouton "exporter vers csv"
            $scope.onBtExport = function() {
                $scope.gridOptions.api.exportDataAsCsv();
            }

        });
})();
