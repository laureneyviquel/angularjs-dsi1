/**
*
* Controlleur de l'onglet matériel
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-05-03
*/


(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("materielCtrl", function($scope, $state, $http, $location, AuthenticationService, $mdDialog, $localStorage) {


            // get the json data from the Q1 excel file passed throw the back end thanks to the factories
            AuthenticationService.getObjets().then(function(response) {
                $scope.objets = response.data.response;
            });


            // supporting reference data pour les sites de l'EPF
            var siteMappings = {
                "1": "Sceaux",
                "2": "Troyes",
                "3": "Montpellier"
            };

            // supporting reference data pour stock ou pret de l'EPF
            var isStockMappings = {
                "0": "Prêt",
                "1": "Stock"
            };

            // Initialize the grid from the library : ag-grid
            var columnDefs = [{
                headerName: "Id matériel",
                field: "id"
            }, {
                headerName: "Catégorie",
                field: "Categorie Nom"
            }, {
                headerName: "Inventaire",
                field: "isStock",
                refData: isStockMappings // just required to specify refData!
            }, {
                headerName: "Site",
                field: "siteEPF",
                refData: siteMappings // just required to specify refData!
            }, {
                headerName: "Commentaire",
                field: "commentaire"
            }];



            // Configurations de l'ag-grid
            $scope.gridOptions = {
                debug: true,
                angularCompileRows: true,
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                enableColResize: true,
                enableFilter: true,
                floatingFilter: true,
                domLayout: 'autoHeight',

                onCellClicked: function(event) { // Lorqu'on clique sur une cellule du tableau
                    console.log(event);

                    $localStorage.objet = event.data;

                    if ($localStorage.objet.isStock === 1) { //le matériel est dans l'inventaire stock
                        $scope.controllerCtr = DialogControllerStock;
                        $scope.template = 'components/alerte/dialogHistoriqueStock.tpl.html';

                    } else { //le matériel est dans l'inventaire prêt
                        $scope.controllerCtr = DialogControllerPret;
                        $scope.template = 'components/alerte/dialogHistoriquePret.tpl.html';
                    }

                    $mdDialog.show({ // affiche la boxe de dialogue lorsqu'on clique sur une cellule du tabeau
                        controller: $scope.controllerCtr,
                        templateUrl: $scope.template, // correpond à la page html quand la boxe de dialogue est appelée
                        parent: angular.element(document.body),
                        targetEvent: event,
                        clickOutsideToClose: true,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                }
            };

            // Le controlleur de la boxe de dialogue pour le stock
            function DialogControllerStock($scope, $mdDialog, $localStorage, AuthenticationService) {
                console.log("Stock alerte dans dialog");

                // Récupére l'historique de stock correspondant à l'objet de l'alerte
                AuthenticationService.getHistorique($localStorage.objet.id).then(function(response) {
                    $scope.historiqueStocks = response.data.response.historiquestock;
                });

                // supporting reference data pour les sites de l'EPF
                var siteMappings = {
                    "1": "Sceaux",
                    "2": "Troyes",
                    "3": "Montpellier"
                };

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Id matériel",
                    field: "idObjet",
                    width: 110
                }, {
                    headerName: "Site",
                    field: "siteEPF",
                    width: 110,
                    refData: siteMappings // just required to specify refData!
                },{
                    headerName: "Arrivée du stock",
                    field: "arrivée",
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Départ du stock",
                    field: "depart",
                    valueFormatter: dateFormatter
                }];

                // Configurations de l'ag-grid
                $scope.gridOptions = {
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                };

                // Initialize les données du tableaux via l'api du back-end
                AuthenticationService.getHistorique($localStorage.objet.id).then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response.historiquestock);
                });

                  // permet de formater la date
                function dateFormatter(params) {
                  var date = new Date(params.value);
                  var returnValue = date.toLocaleString()
                  if (params.value === null) {
                    return " ";
                  } else {
                    return returnValue;
                  }
                }

                // Les fonctions pour pouvoir fermer la boxe de dialogue
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.answer = function() {
                    $mdDialog.hide();
                };
            }

            // Le controlleur de la boxe de dialogue pour le pret
            function DialogControllerPret($scope, $mdDialog, $localStorage, AuthenticationService) {
                console.log("Pret alerte dans dialog");

                // Récupére l'historique de pret correspondant à l'objet de l'alerte
                AuthenticationService.getHistorique($localStorage.objet.id).then(function(response) {
                    $scope.historiquePrets = response.data.response.historiquepret;
                });

                // supporting reference data pour les sites de l'EPF
                var siteMappings = {
                    "1": "Sceaux",
                    "2": "Troyes",
                    "3": "Montpellier"
                };

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Matériel",
                    field: "idObjet",
                    width: 90
                }, {
                    headerName: "Site",
                    field: "siteEPF",
                    width: 80,
                    refData: siteMappings // just required to specify refData!
                }, {
                    headerName: "Départ",
                    field: "depart",
                    width: 165,
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Retour prévu",
                    field: "retourPrevu",
                    width: 165,
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Retour effectif",
                    field: "retourEffectif",
                    width: 165,
                    valueFormatter: dateFormatter
                }];

                // Configurations de l'ag-grid
                $scope.gridOptions = {
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                };

                // Initialize les données du tableaux via l'api du back-end
                AuthenticationService.getHistorique($localStorage.objet.id).then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response.historiquepret);
                });

                  // permet de formater la date
                function dateFormatter(params) {
                  var date = new Date(params.value);
                  var returnValue = date.toLocaleString()
                  if (params.value === null) {
                    return " ";
                  } else {
                    return returnValue;
                  }
                }


                // Les fonctions pour pouvoir fermer la boxe de dialogue
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.answer = function() {
                    $mdDialog.hide();
                };
            }


            // Initialize les données du tableaux via l'api du back-end
            AuthenticationService.getObjets().then(function(response) {
                $scope.gridOptions.api.setRowData(response.data.response);
            });

            // exporter les données dans le tableau en csv, lorsqu'on clique sur le bouton "exporter vers csv"
            $scope.onBtExport = function() {
                $scope.gridOptions.api.exportDataAsCsv();
            }



        });
})();
