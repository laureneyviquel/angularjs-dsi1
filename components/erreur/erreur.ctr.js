/**
*
* Controlleur de la boite de dialogue alerte
* est appelé lorsque le state correspond à 'erreur'
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-05-01
*/

(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("erreurCtrl", function($scope, $state, $http, AuthenticationService, $mdDialog, $location) {

          $mdDialog.show({
              controller: DialogControllerStock,
              templateUrl: 'components/erreur/erreur.tpl.html', // correpond à la page html quand la boxe de dialogue est appelée
              parent: angular.element(document.body),
              targetEvent: event,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
          })

          // Le controlleur de la boxe de dialogue pour le stock
          function DialogControllerStock($scope, $mdDialog, $localStorage, AuthenticationService) {
              console.log("Stock alerte dans dialog");

              // Les fonctions pour pouvoir fermer la boxe de dialogue
              $scope.cancel = function() { // lorsqu'on clique sur le bouton "retour accueil"
                  $mdDialog.cancel(); //ferme la box de dialogue
                  $location.path('/'); // retourne à la page d'acceuil
              };
          }



        });
})();
