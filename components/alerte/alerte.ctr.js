/**
*
* Controlleur de l'onglet alerte
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-18
*/

(function() {

    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    angular
        .module("ngMateriels")
        .controller("alerteCtrl", function($scope, $state, $http, AuthenticationService, $mdDialog, $mdSidenav, $window, $localStorage) {

          if ($localStorage.currentUser.role === 1 || $localStorage.currentUser.role === 2) {
            // récupérer les alertes stock et pret de la factorie : AuthenticationService
            AuthenticationService.getAlertes().then(function(response) {
                $scope.pretAlertes = response.data.response.alertesPret;
                $scope.stockAlertes = response.data.response.alertesStock;
            });


            // Permet d'afficher la boxe de  dialogue en grand
            $scope.customFullscreen = false;

            // Lorsqu'on clique sur la liste d'un matériel en pret --> alors une md-dialogue apparait
            $scope.showHistPret = function(event, pretAlerte) {
                $localStorage.pretAlerte = pretAlerte;

                $mdDialog.show({
                    controller: DialogControllerPret,
                    templateUrl: 'components/alerte/dialogHistoriquePret.tpl.html', // correpond à la page html quand la boxe de dialogue est appelée
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
            }

            // Le controlleur de la boxe de dialogue pour le pret
            function DialogControllerPret($scope, $mdDialog, $localStorage, AuthenticationService) {
                console.log("Pret alerte dans dialog");

                // Récupére l'historique de pret correspondant à l'objet de l'alerte
                AuthenticationService.getHistorique($localStorage.pretAlerte.idObjet).then(function(response) {
                    $scope.historiquePrets = response.data.response.historiquepret;
                });

                // supporting reference data pour les sites de l'EPF
                var siteMappings = {
                    "1": "Sceaux",
                    "2": "Troyes",
                    "3": "Montpellier"
                };

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Matériel",
                    field: "idObjet",
                    width: 90
                }, {
                    headerName: "Site",
                    field: "siteEPF",
                    width: 80,
                    refData: siteMappings // just required to specify refData!
                }, {
                    headerName: "Départ",
                    field: "depart",
                    width: 165,
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Retour prévu",
                    field: "retourPrevu",
                    width: 165,
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Retour effectif",
                    field: "retourEffectif",
                    width: 165,
                    valueFormatter: dateFormatter
                }];

                // Configurations de l'ag-grid
                $scope.gridOptions = {
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                };

                // Initialize les données du tableaux via l'api du back-end
                AuthenticationService.getHistorique($localStorage.pretAlerte.idObjet).then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response.historiquepret);
                });

                  // permet de formater la date
                function dateFormatter(params) {
                  var date = new Date(params.value);
                  var returnValue = date.toLocaleString()
                  if (params.value === null) {
                    return " ";
                  } else {
                    return returnValue;
                  }
                }

                // Les fonctions pour pouvoir fermer la boxe de dialogue
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.answer = function() {
                    $mdDialog.hide();
                };
            }

            // Lorsqu'on clique sur la liste d'un matériel en stock --> alors une md-dialogue apparait
            $scope.showHistStock = function(event, stockAlerte) {
                $localStorage.stockAlerte = stockAlerte;

                $mdDialog.show({
                    controller: DialogControllerStock,
                    templateUrl: 'components/alerte/dialogHistoriqueStock.tpl.html', // correpond à la page html quand la boxe de dialogue est appelée
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
            }

            // Le controlleur de la boxe de dialogue pour le stock
            function DialogControllerStock($scope, $mdDialog, $localStorage, AuthenticationService) {
                console.log("Stock alerte dans dialog");

                // Récupére l'historique de stock correspondant à l'objet de l'alerte
                AuthenticationService.getHistorique($localStorage.stockAlerte.idObjet).then(function(response) {
                    $scope.historiqueStocks = response.data.response.historiquestock;
                });

                // supporting reference data pour les sites de l'EPF
                var siteMappings = {
                    "1": "Sceaux",
                    "2": "Troyes",
                    "3": "Montpellier"
                };

                // Initialize the grid from the library : ag-grid
                var columnDefs = [{
                    headerName: "Id matériel",
                    field: "idObjet",
                    width: 110
                }, {
                    headerName: "Site",
                    field: "siteEPF",
                    width: 110,
                    refData: siteMappings // just required to specify refData!
                },{
                    headerName: "Arrivée du stock",
                    field: "arrivée",
                    valueFormatter: dateFormatter
                }, {
                    headerName: "Départ du stock",
                    field: "depart",
                    valueFormatter: dateFormatter
                }];

                // Configurations de l'ag-grid
                $scope.gridOptions = {
                    debug: true,
                    angularCompileRows: true,
                    columnDefs: columnDefs,
                    rowData: null,
                    enableSorting: true,
                    enableColResize: true,
                    enableFilter: true,
                    floatingFilter: true,
                    domLayout: 'autoHeight',
                };

                // Initialize les données du tableaux via l'api du back-end
                AuthenticationService.getHistorique($localStorage.stockAlerte.idObjet).then(function(response) {
                    $scope.gridOptions.api.setRowData(response.data.response.historiquestock);
                });

                  // permet de formater la date
                function dateFormatter(params) {
                  var date = new Date(params.value);
                  var returnValue = date.toLocaleString()

                  if (params.value === null) {
                    return " ";
                  } else {
                    return returnValue;
                  }
                }

                // Les fonctions pour pouvoir fermer la boxe de dialogue
                $scope.hide = function() {
                    $mdDialog.hide();
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.answer = function() {
                    $mdDialog.hide();
                };
            }



            // Supprimer l'objet (qui correspond à l'alerte) car il a potentiellement été volé
            $scope.deleteObjet = function(event, user) {
                // Fenetre de dialogue pour confirmer si supprésion de l'objet
                var confirm = $mdDialog.confirm()
                    .title('Etes-vous sûr de vouloir supprimer l objet : ' + user.idObjet + '?')
                    .ok('Oui')
                    .cancel('Non')
                    .targetEvent(event);

                $mdDialog.show(confirm).then(function() {
                    console.log("supprime id : " + user.idObjet);
                    //appelle la fonction http du back-end pour supprimer l'objet, en passant son id en paramètre

                    AuthenticationService.deleteObjet(user.idObjet).then(function(response) {
                        console.log(response);
                    });

                    //$window.location.reload();
                }, function() {});
            }


          } else {
            console.log("Controlleur non authorisé");
          }

        });
})();
