/**
* Permet de gérer et de définir toutes les routes de l'application angularJS
* On y définit l'url, la page html, le controlleur et le state associé pour toutes les pages
*
* @author  Laurène Yviquel
* @version 1.0
* @since   2018-04-07
*/

angular
    .module("ngMateriels", ["ngMaterial", "ui.router", "ngFileUpload", "agGrid", "ngStorage", "ngMessages"])
    .config(config)
    .run(run);

function config($mdThemingProvider, $stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    // Setup the theme of the app
    $mdThemingProvider.theme('default')
        .primaryPalette('red')
        .accentPalette('blue-grey');

    // if the path doesn't match any of the urls you configured
    // otherwise will take care of routing the user to the specified url : /home
    $urlRouterProvider.otherwise('/');

    // Configuration with stateProvider for the state, Url, template HTMP and the javascript controller
    $stateProvider
        .state('home', { //onglet accueil
            url: '/', // affiche cette url dans le navigateur
            templateUrl: 'components/home/home.tpl.html', // appelle cette page html
            controller: 'homeCtrl', // appelle ce controller
            controllerAs: 'vm' // ce controlleur peut etre définit par vm
        })
        .state('login', { // onglet se connecter
            url: '/login',
            templateUrl: 'components/login/login.tpl.html',
            controller: 'loginCtrl',
            controllerAs: 'vm'
        })
        .state('user', { // onglet utilisateur
            url: '/user',
            templateUrl: 'components/user/user.tpl.html',
            controller: 'userCtrl',
            onEnter: function($localStorage, $state) { //si l'utilisateur est n'est pas la DSI alors il est redirigé vers une page d'erreur
                if ($localStorage.currentUser.role === 3 || $localStorage.currentUser.role === 2) {
                    $state.go('erreur');
                }
            }
        })
        .state('alerte', {
            url: '/alerte',
            templateUrl: 'components/alerte/alerte.tpl.html',
            controller: 'alerteCtrl',
            onEnter: function($localStorage, $state) { //si l'utilisateur est l'Administration alors il est redirigé vers une page d'erreur
                if ($localStorage.currentUser.role === 3) {
                    $state.go('erreur');
                }
            }
        })
        .state('stock', {
            url: '/stock',
            templateUrl: 'components/stock/stock.tpl.html',
            controller: 'stockCtrl',
            onEnter: function($localStorage, $state) { //si l'utilisateur est l'Administration alors il est redirigé vers une page d'erreur
                if ($localStorage.currentUser.role === 3) {
                    $state.go('erreur');
                }
            }
        })
        .state('pret', {
            url: '/pret',
            templateUrl: 'components/pret/pret.tpl.html',
            controller: 'pretCtrl',
            onEnter: function($localStorage, $state) { //si l'utilisateur est l'Administration alors il est redirigé vers une page d'erreur
                if ($localStorage.currentUser.role === 3) {
                    $state.go('erreur');
                }
            }
        })
        .state('materiel', {
            url: '/materiel',
            templateUrl: 'components/materiel/materiel.tpl.html',
            controller: 'materielCtrl',
            onEnter: function($localStorage, $state) { //si l'utilisateur est l'Administration alors il est redirigé vers une page d'erreur
                if ($localStorage.currentUser.role === 3) {
                    $state.go('materielAdmin');
                }
            }
        })
        .state('materielAdmin', {
            url: '/materielAdmin',
            templateUrl: 'components/materiel/materielAdmin.tpl.html',
            controller: 'materielAdminCtrl',
        })
        .state('erreur', {
            url: '/erreur',
            templateUrl: 'components/erreur/pageBlanche.tpl.html',
            controller: 'erreurCtrl',
        });

}

// gère les accès aux pages de l'application seulement si l'utilisateur est bien authentifié
function run($rootScope, $http, $location, $localStorage) {
    // keep user logged in after page refresh
    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        var publicPages = ['/login'];
        var restrictedPage = publicPages.indexOf($location.path()) === -1;
        if (restrictedPage && !$localStorage.currentUser) {
            $location.path('/login');
        }
    });
}
